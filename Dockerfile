FROM mhart/alpine-node:6.3.0

RUN npm install -g swagger

RUN mkdir -p /usr/src/app/customer-service
WORKDIR /usr/src/app/customer-service

COPY package.json .
RUN npm install --production

COPY app.js .
COPY config config/
COPY api api/

EXPOSE 8080

CMD node app.js -l dev
