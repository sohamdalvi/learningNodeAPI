'use strict';

let
    appRootDir = require('app-root-dir').get(),
    appConfig = require('config'),
    cors = require('cors'),
    app = require('express')(),
    logger = require('morgan'),
    SwaggerExpress = require('swagger-express-mw'),
    SwaggerUi = require('swagger-tools/middleware/swagger-ui');

const argv = require('yargs')
    .usage('$0 -l [format]')
    .alias('l','logs')
    .describe('l','Morgan log format: combined, common, dev, short, tiny')
    .nargs('l',1)
    .help('h')
    .alias('h','help')
    .strict()
    .argv;

const Connection = require(appRootDir + '/api/db/db-connection.js');

const connection = Connection({
    host: appConfig.get('mongoConfig.host'),
    dbname: appConfig.get('mongoConfig.db'),
    user: appConfig.get('mongoConfig.username'),
    pass: appConfig.get('mongoConfig.passwd')
    // hosts: "10.10.11.132:27017",
    // dbname: "BSSEXCLM"
});

let swaggerConfig = {
    appRoot: __dirname // required config
};

// enables try this in swagger docs
app.use(cors());

SwaggerExpress.create(swaggerConfig, function(err, swaggerExpress) {

    if (err) { throw err; }

    // Add swagger-ui (This must be before swaggerExpress.register)
    app.use(SwaggerUi(swaggerExpress.runner.swagger));

    if (argv.l) {
        app.use(logger(argv.l));
    }
    // -l <arg> enables simple logger for http requests
    // <arg> can be one of standard Morgan formats: combined, common, dev, short, tiny

    // install middleware
    swaggerExpress.register(app);

    const port = appConfig.get('port') || 10010;

    connection.connect()
        .then(() => {
            app.listen(port);
            if (swaggerExpress.runner.swagger.paths['/customers']) {
                console.log(`Server listening on http://localhost:${port}`);
            }
        });
});

module.exports = app;