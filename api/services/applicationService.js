'use strict';

const
    _ = require('lodash'),
    appRootDir = require('app-root-dir').get(),
    debug = require('debug')('corp-services'),
    logger = require(appRootDir + '/api/util/logger'),
    mongoose = require(appRootDir + '/api/db/db.js').mongoose,
    util = require(appRootDir + '/api/helpers/util.js');

function postTravelRequest(travelRequestInput){

    return new Promise(function (resolve,reject){
        const
            travelRequests = mongoose.connection.db.collection('travelRequests');

        var travelRequestNo = "10"+util.generateRandomNumber(Math.floor(Math.random()*159));

        logger.info("travelRequestNo "+ travelRequestNo);
        travelRequestInput.travelRequestNo = travelRequestNo;

        logger.info("travelRequest Data "+JSON.stringify(travelRequestInput));
        travelRequests
            .insert(travelRequestInput)
            .then(result => {
                logger.info("travelRequestInput inserted "+JSON.stringify(result));
                logger.info("Success ");
                resolve({
                            "travelRequestNo": travelRequestNo
                        });                
            }).catch(err => {
                logger.error(err);
                reject(err);
        });
    });
};

function getTravelRequest(travelRequestInput){

    return new Promise(function (resolve,reject){
        const
            travelRequests = mongoose.connection.db.collection('travelRequests');
        logger.info("log1");            
        travelRequests
            .find({ "customerName": "Suhas Dalvi" } )
            .then(result => {
                logger.info("search Success ");
                logger.info("travelRequestInput inserted "+JSON.stringify(result));
                resolve(result);                
            }).catch(err => {
                logger.error(err);
                reject(err);
        });
    });
};

module.exports = {
    postTravelRequest: postTravelRequest,
    getTravelRequest : getTravelRequest
}
