'use strict';

const
    _ = require('lodash'),
    appRootDir = require('app-root-dir').get(),
    debug = require('debug')('corp-services'),
    logger = require(appRootDir + '/api/util/logger'),
    mongoose = require(appRootDir + '/api/db/db.js').mongoose,
    util = require(appRootDir + '/api/helpers/util.js');

function postCustomers(shopData){

    return new Promise(function (resolve,reject){
        const
            Services = mongoose.connection.db.collection('Service'),
            Profile =  mongoose.connection.db.collection('Profile'),
            BillingAccount = mongoose.connection.db.collection('BillingAccount');

        var customerCode = "10"+util.generateRandomNumber(Math.floor(Math.random()*159));
        var accountCode = "00"+util.generateRandomNumber(Math.floor(Math.random()*8137));
        var idNumber = "adbg-"+util.generateRandomNumber(Math.floor(Math.random()*5213));
        var serviceRequestId ="201700"+util.generateRandomNumber(Math.floor(Math.random()*361));

        logger.info("customerCode "+customerCode);
        shopData.customerCode = customerCode;
        shopData.accountCode = accountCode;
        shopData.idNumber = idNumber;
        shopData.serviceCode = idNumber;
        shopData.serviceRequestId = serviceRequestId;

        logger.info("shop Data "+JSON.stringify(shopData));
        var serviceData = util.mapService(shopData);
        logger.info("serviceData "+JSON.stringify(serviceData));
        var billingAccountData = util.mapBillingAccount(shopData);
        logger.info("billing Account "+JSON.stringify(billingAccountData));
        var profileData = util.mapProfile(shopData);
        logger.info("profileData "+JSON.stringify(profileData));
        Services
            .insert(serviceData)
            .then(result => {
                logger.info("Service record created "+JSON.stringify(result));
                Profile.insert(profileData).then(profile => {
                    logger.info("Profile record created "+JSON.stringify(profile));
                    BillingAccount.insert(billingAccountData).
                    then(account =>{
                        logger.info("Billing Account record created "+JSON.stringify(account));
                        logger.info("Success ");
                        resolve({
                            "customerCode": customerCode,
                            "accountCode":accountCode
                        });
                    });
                });
            }).catch(err => {
                logger.error(err);
                reject(err);
        });
    });
}

module.exports = {
    postCustomers: postCustomers
}
