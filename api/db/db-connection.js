'use strict';

const
    appRootDir = require('app-root-dir').get(),
    debug = require('debug')('customer-service'),
    logger = require(appRootDir + '/api/util/logger');

function dbConnection(options) {

    const mongoose = options.mongoose || require('./db').mongoose;
    options.promiseLibrary = require('bluebird');

    debug('Mongo db connection options: ', options);

    return {
        connect () {
            const db = mongoose.connection;
            db.on('error', (err) => {
                logger.error(err, 'Mongo connection error');
            });
            db.once('open', () => {
                debug('Connected to mongo');
            });
            debug(`mongodb://${options.host}/${options.dbname}`);
            return mongoose.connect(`mongodb://${options.host}/${options.dbname}`, options)
                .catch((err) => {
                    logger.error(err, 'Error connecting to mongoose');
                    throw err;
                });
        }
    };
}

module.exports = dbConnection;
