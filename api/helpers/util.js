
'use strict';

const
    _ = require('lodash'),
    appRootDir = require('app-root-dir').get(),
    logger = require(appRootDir + '/api/util/logger'),
    moment = require('moment');

function mapProfile(shopData) {
    return {
        "profileDetails": {
            "serviceRequestId": shopData.serviceRequestId,
            "profileStatus": {
                "masterCode": "ACT"
            },
            "customerSubCategory": {
                "masterCode": "Local"
            },
            "identificationDetails": {
                "identificationDetail": [
                    {
                        "idType": {
                            "masterCode": "IDCARD"
                        },
                        "documentPurpose": {
                            "masterCode": "POID"
                        },
                        "idNumber": shopData.idNumber
                    }
                ]
            },
            "organization": "operator",
            "customerType": {
                "masterCode": "I"
            },
            "customerCategory": {
                "masterCode": "SC00001"
            },
            "notificationDetails": {
                "isDNDApplicable": "N",
                "preferredMedium": {
                    "isContactByEmail": "Y",
                    "emailId": shopData.email,
                    "isContactByPost": "N",
                    "isContactBySms": "N",
                    "isContactByPhone": "N",
                    "isContactByFax": "N"
                },
                "preferredLanguage": {
                    "masterCode": "en"
                }
            },
            "basicDetails": {
                "VIP": "N",
                "dateOfBirth": "",
                "lastName": shopData.lastName,
                "title": "",
                "nationality":"",
                "idCheckStatus": "PASSED",
                "customerCode": shopData.customerCode,
                "customerFullName": shopData.firstName + " "+shopData.lastName,
                "gender": "",
                "blackListCheckStatus": "PASSED",
                "firstName": shopData.firstName,
                "country": {
                    "masterCode": "FIN"
                }
            },
            "registrationDetails": {
                "registrationDate": moment().toISOString(),
                "userDetails": {
                    "userName": "CLM User",
                    "loginName": "clmui"
                },
                "salesChannel": {
                    "masterCode": "CLM"
                }
            },
            "addresses": {
                "addressDetails": [
                    {
                        "addressValidationStatus": "PASSED",
                        "zipCode": shopData.postalCode,
                        "province": {
                            "masterCode": shopData.region
                        },
                        "streetName": shopData.address1,
                        "addressType": {
                            "masterCode": "Residence"
                        },
                        "addressFormat": "STRA",
                        "city": {
                            "masterCode": shopData.city
                        },
                        "country": {
                            "masterCode": shopData.country
                        }
                    }
                ],
                "preferredDeliveryMode": {
                    "masterCode": "NA"
                }
            },
            "riskDetails": {
                "riskCategory": {
                    "masterCode": "Low"
                }
            },
            "createdDate": moment().toISOString(),
            "modifiedDate": moment().toISOString(),
            "activationDate": moment().toISOString(),
            "xDirLevel": {
                "masterCode": 1
            }
        }
    }
}

function mapBillingAccount(shopData){
    return {
        "billingAccount" : {
            "serviceRequestId" : shopData.serviceRequestId,
            "billingPreferenceDetails" : {
                "presentationLanguage" : {
                    "masterCode" : "en"
                },
                "billDispatchDetails" : {
                    "isBillByPost" : "Y",
                    "isBillByFax" : "N",
                    "isBillByEmail" : "N"
                },
                "prefferedCurrency" : {
                    "masterCode" : "EUR"
                }
            },
            "preferredDeliveryMode" : {
                "masterCode" : "NA"
            },
            "notificationDetails" : {
                "isDNDApplicable" : "N",
                "preferredMedium" : {
                    "isContactByEmail" : "Y",
                    "email":shopData.email,
                    "isContactByPost" : "N",
                    "isContactBySms" : "N",
                    "isContactByPhone" : "N",
                    "isContactByFax" : "N"
                },
                "preferredLanguage" : {
                    "masterCode" : "en"
                }
            },
            "billingDetails" : {
                "isTaxApplicable" : "Y",
                "isDunningApplicable" : "Y",
                "accountStatus" : {
                    "masterCode" : "ACT"
                },
                "accountType" : {
                    "masterCode" : "CB"
                },
                "subsidiary" : {
                    "masterCode" : "UKKO"
                },
                "dunningScheduleCode" : "DUN03"
            },
            "accountIndex" : "02cafa3d-50b1-404d-a4f7-726a6b33fc10",
            "accountCode" : shopData.accountCode,
            "organization" : "operator",
            "customerCode" : shopData.customerCode,
            "billingAddressDetails" : {
                "addressValidationStatus" : "PASSED",
                "zipCode" : shopData.postalCode,
                "province" : {
                    "masterCode" : shopData.region
                },
                "streetName" : shopData.address1,
                "addressType" : {
                    "masterCode" : "Billing"
                },
                "addressFormat" : "STRA",
                "city" : {
                    "masterCode" : shopData.city
                },
                "country" : {
                    "masterCode" : shopData.country
                }
            },
            "isAutoDebitApplicable" : "N",
            "registrationDetails" : {
                "registrationDate" : moment().toISOString(),
                "userDetails" : {
                    "userName" : "clm user",
                    "loginName" : "test"
                },
                "salesChannel" : ""
            },
            "accountOwnerDetails" : {
                "dateOfBirth" : "",
                "lastName" : shopData.lastName,
                "title" : {
                    "masterCode" : "MR"
                },
                "nationality" : "",
                "customerFullName" : shopData.firstName + " "+shopData.lastName,
                "gender" : "",
                "firstName" : shopData.firstName,
                "country" : {
                    "masterCode" : shopData.country
                }
            },
            "billCycleDetails" : {
                "billCycle" : {
                    "masterCode" : "d906c864-e4ff-4888-a960-841806388981"
                },
                "billPeriodicity" : {
                    "masterCode" : "MONTHLY"
                }
            },
            "createdDate" : moment().toISOString(),
            "modifiedDate" : moment().toISOString(),
            "activationDate" : moment().toISOString()
        }
    }
}

function mapService(shopData){

    logger.info("shopData "+shopData);

    var temp = {
        "service" : {
            "serviceRequestId" : shopData.serviceRequestId,
            "serviceIndex" : "S0",
            "accountIndex" : "f964012f-a30e-4bf5-89f9-265bb66e33c8",
            "accountCode" : shopData.accountCode,
            "organization" : "operator",
            "serviceUser" : {
                "isServiceUserSameAsCustomer" : "Y",
                "profileDetails" : {
                    "identificationDetails": {
                        "identificationDetail": [
                            {
                                "idType": {
                                    "masterCode": "IDCARD"
                                },
                                "documentPurpose": {
                                    "masterCode": "POID"
                                },
                                "idNumber":shopData.idNumber
                            }
                        ]
                    },
                    "notificationDetails": {
                        "isDNDApplicable": "N",
                        "preferredMedium": {
                            "isContactByEmail": "Y",
                            "emailId": shopData.email,
                            "isContactByPost": "N",
                            "isContactBySms": "N",
                            "isContactByPhone": "Y",
                            "isContactByFax": "N"
                        },
                        "preferredLanguage": {
                            "masterCode": "en"
                        }
                    },
                    "basicDetails": {
                        "dateOfBirth": "",
                        "lastName": shopData.lastName,
                        "title": "",
                        "nationality":"",
                        "idCheckStatus": "PASSED",
                        "customerFullName": shopData.firstName + " "+shopData.lastName,
                        "gender": "",
                        "firstName": shopData.firstName,
                        "country": {
                            "masterCode": shopData.country
                        }
                    },
                    "addresses": {
                        "addressDetails": [
                            {
                                "zipCode": shopData.postalCode,
                                "province": {
                                    "masterCode": shopData.region
                                },
                                "streetName": shopData.address1,
                                "addressType": {
                                    "masterCode": "Residence"
                                },
                                "addressFormat": "STRA",
                                "city": {
                                    "masterCode": shopData.city
                                },
                                "country": {
                                    "masterCode": shopData.country
                                }
                            }
                        ],
                        "preferredDeliveryMode": {
                            "masterCode": "NA"
                        }
                    }
                },
                "createdDate" :moment().toISOString(),
            },
            "serviceDetails" : {
                "continueService" : "YES",
                "selfCareAccount" : "Y",
                "serviceNumberCategory" : {
                    "serviceNumberCategory" : "NORM1"
                },
                "contractDetails" : {
                    "startDate" : moment().toISOString(),
                    "endDate" :  moment().add(90,'days').toISOString()
                },
                "technology" : {
                    "masterCode" : "LTE450"
                },
                "businessType" : {
                    "masterCode" : "Postpaid"
                },
                "additionalCreditLimit" : "",
                "activatedVia" : {
                    "masterCode" : "SC"
                },
                "serviceType" : {
                    "masterCode" : "GSM"
                },
                "mobileMoneyAccount" : "Y",
                "subServiceType" : {
                    "masterCode" : "Data"
                },
                "simDetails" : {
                    "imsiNumber" : "244350010000012",
                    "simCategory" : {
                        "masterCode" : "multi"
                    },
                    "simNumber" : "8935811000010000012"
                },
                "limitDetails" : "",
                "serviceNumber" : "244350010000012",
                "serviceStatus" : {
                    "masterCode" : "INPROGRESS"
                },
                "registrationDetails" : {
                    "registrationDate" : moment().toISOString(),
                    "userDetails" : {
                        "userName" : "temp test",
                        "loginName" : "test"
                    },
                    "salesChannel" : {
                        "masterCode" : "CLM"
                    }
                }
            },
            "customerCode" : shopData.customerCode,
            "isDeliveryRequired" : "N",
            "serviceCode" : shopData.serviceCode, //"42c57063-c1da-48f4-896e-9e667285745d",
            "offerings" : {
                "offering" : {
                    "id" : "57e3db18498ecf087830c587",
                    "serviceIndex" : [
                        "S0",
                        "S0"
                    ],
                    "accountCode" : shopData.accountCode,
                    "serviceNumber" : "244350010000012",
                    "offeringType" : {
                        "masterCode" : "Plan"
                    },
                    "customerCode" : shopData.customerCode,
                    "name" : "Ukko LTE Offering",
                    "serviceCode" : shopData.serviceCode, //"42c57063-c1da-48f4-896e-9e667285745d",
                    "code" : "LTE01",
                    "offeringCharges" : {
                        "offeringCharge" : [
                            {
                                "chargePeriodicity" : 1,
                                "taxAmount" : "2.10",
                                "isRefundable" : "N",
                                "chargeDescription" : "Initial fee",
                                "waivedAmount" : 0,
                                "chargeInclTax" : 17.09,
                                "collectUpfront" : "N",
                                "refundableAmount" : 0,
                                "isWaived" : "N",
                                "chargeType" : "OneTimeCharge",
                                "chargeCode" : "b99a9227-620a-4ee9-9548-3f59008c4dfe",
                                "isByInstallment" : "N",
                                "numberOfInstallments" : 0,
                                "chargeExclTax" : 14.99
                            }
                        ]
                    },
                    "version" : "1.0",
                    "currency" : {
                        "masterCode" : "EUR"
                    }
                }
            },
            "createdDate" : moment().toISOString(),
            "modifiedDate" : moment().toISOString()
        }
    }
    logger.info("temp "+JSON.stringify(temp));
    return temp;
}

function generateRandomNumber(index){
    return Math.floor(Math.random()* 700000)+index;
}


module.exports = {
    mapBillingAccount : mapBillingAccount,
    generateRandomNumber: generateRandomNumber,
    mapProfile: mapProfile,
    mapService: mapService
};