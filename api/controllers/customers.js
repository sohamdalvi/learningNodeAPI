'use strict';

const
    appRootDir = require('app-root-dir').get(),
    logger = require(appRootDir + '/api/util/logger'),
    clmService = require('../services/clmservice'),
    HttpStatus = require('http-status-codes');

function postCustomers(req, res) {
    // variables defined in the Swagger document can be referenced using req.swagger.params.{parameter_name}
    var customerObj = req.swagger.params.customer.value ;

    clmService.postCustomers(customerObj)
        .then(result => {
            if (result) {
                return res.status(HttpStatus.CREATED).json(result);
            } else {
                res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({"msg":"FAILED"});
            }
        })
        .catch(err =>  res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err));

}

module.exports = {
    postCustomers: postCustomers
}