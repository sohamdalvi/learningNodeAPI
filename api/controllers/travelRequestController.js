'use strict';

const
    appRootDir = require('app-root-dir').get(),
    logger = require(appRootDir + '/api/util/logger'),
    applicationService = require('../services/applicationService'),
    HttpStatus = require('http-status-codes');

function postTravelRequest(req, res) {
    // variables defined in the Swagger document can be referenced using req.swagger.params.{parameter_name}
    var travelRequestObj = req.swagger.params.travelRequest.value ;

    applicationService.postTravelRequest(travelRequestObj)
        .then(result => {
            if (result) {
                return res.status(HttpStatus.CREATED).json(result);
            } else {
                res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({"msg":"FAILED"});
            }
        })
        .catch(err =>  res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err));
}

function getTravelRequest(req, res) {
    // variables defined in the Swagger document can be referenced using req.swagger.params.{parameter_name}
//    var travelRequestObj = req.swagger.params.travelRequest.value ;
    var travelRequestObj = {};
     
    applicationService.getTravelRequest(travelRequestObj)
        .then(result => {
            if (result) {
                return res.status(HttpStatus.CREATED).json(result);
            } else {
                res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({"msg":"FAILED"});
            }
        })
        .catch(err =>  res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err));
}

module.exports = {
    postTravelRequest: postTravelRequest,
    getTravelRequest : getTravelRequest
}